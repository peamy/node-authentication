const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const expressValidator = require('express-validator');
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);

const register = require('./pages/register');
const login = require('./pages/login');
const dbConnection = require('./database/database-connection');

var errors;

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended:false}));
router.use(expressValidator());
router.use(session({
    secret: '1WIqztV5ymIlhFO07LKfOjt0uc1Vkp0rNV3hmSkxDwEngcGlSEmhwa5GIpXW1234',
    saveUninitialized: false,
    resave: false,
    store: new MongoStore({ mongooseConnection: dbConnection })
}));

//routing
router.get('/', (req, res) => {
  if(req.session.login) {
    res.render('index', {
      email: req.session.email
    });
  }
  else {
    res.render('login')
  }
});

router.post('/logout', (req, res) => {
  req.session.login = false
  res.redirect('/');
})

router.get('/login', (req, res) => {
  res.render('login', {
    errors : errors
  });
});

router.post('/login', (req, res) => {
  req.check('email', 'Invalid email adress').isEmail();

  login.validateLogin({email: req.body.email, password: req.body.password}, function(user) {
    if(user) {
      req.session.login = true;
      req.session.email = req.body.email;
      res.redirect('/')
    }
    else {
      var error = {msg: `Email and password do not match`}
      errors = { error };

      req.session.login = false;
      res.redirect('/login')
    }
  });

})

router.get('/register', (req, res) => {
  res.render('register', {
    errors: errors
  });
});

router.post('/register', (req, res) => {
  req.check('email', 'Invalid email adress').isEmail();
  req.check('password', 'Password is too short').isLength({ min:4 });
  req.check('password', 'Passwords don\'t match').equals(req.body.validatePassword);

  errors = req.validationErrors();

  if(errors) {
    res.redirect('/register');
  }
  else {
    register.registerUser({email: req.body.email, password: req.body.password, validatePassword: req.body.validatePassword}, (err) => {
      if(err) {
        var error = {msg: `Account with email ${req.body.email} already exists`}
        errors = { error };
        res.redirect('/register');
      }
      else {
        res.redirect('/')
      }
    });
  }
})


module.exports = router;
