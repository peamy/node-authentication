const dbUser = require('../database/database-querys');
const bcrypt = require('bcrypt');
const saltRounds = 10;

function registerUser (user, callback) {
  if(user)  {
    bcrypt.hash(user.password, saltRounds, function(err, hash) {
      if(err){
        return callback(err);
      }

      user.password = hash;
      dbUser.insertUser(user, (err, newUser) => {
        callback(err);
      });
    })
  }
}

module.exports = {
  registerUser
}
