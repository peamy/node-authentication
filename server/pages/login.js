const dbUser = require('../database/database-querys');
const bcrypt = require('bcrypt');

function validateLogin(user, callback)
{
  dbUser.getUserByEmail(user.email, function(err, person){
    if(err){
      console.log('there is error getting the user');
      return callback()
    }

    if(person) {
      bcrypt.compare(user.password, person.password).then(function(response){
        if(response) {
          console.log('password is ok');
          return callback(user);
        }
        else {
          console.log('password is faulty')
          return callback();
        }
      })
    }
    else {
      console.log('user does not exist')
      return callback()
    }
  })
  //get user from db
  //if user exists, validate user Password
  //if user not exists, throw errors
  //if passwords not match, throw errors
  //if password correct, throw correct
}

module.exports = {
  validateLogin
}
