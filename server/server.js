const express = require('express');
const hbs = require('hbs');

const port = 8080;
const router = require('./router');

var app = express();

//set view engine
app.set('view engine', 'hbs');
hbs.registerPartials(__dirname + './../views/partials');

//router
app.use(router);

app.listen(port, () => {
  console.log(`server has started on port ${port}`);
});
