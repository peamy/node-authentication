const db = require('./database-connection');
const model = require('./models/user');

function insertUser(user, callback){

  var newUser = model.Users(user);

  newUser.save(function (err, newUser) {
      callback(err, newUser);
  })
}

function getUserByEmail(email, callback)
{
  model.Users.findOne({email: email}, function(err, person){    
    callback(err, person);
  });
}

module.exports = {
  insertUser,
  getUserByEmail
}
