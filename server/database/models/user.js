const mongoose = require('mongoose');

var Users = mongoose.model('Users', {
  email: {
    type: String,
    required: true,
    minlength: 6,
    trim: true,
    unique: true
  },
  password:
  {
    type: String,
    required: true,
    minlength: 4,
    maxlength: 60,
    trim: false
  }
});

module.exports = {
  Users
}
